### Moxie WP Test: Caridad

To test the plugin you will need:

- Install [Types](https://wordpress.org/plugins/types/)
- Import the CPT & custom fields definition: [Settings](https://bitbucket.org/caridad/moxie-wp-test/raw/02b136526ce8f11a0ea5844edb17daa878dfe0a1/settings.xml)
- Add some movies

Once you have this, you can clone this repository in the plugins folder of your WordPress folder and activate it.

There are several ways to display the list of movies:

- Now you can create a page and insert the `[movies]` shortcode in it
- You can copy and paste the output of the movies shortcode to your template and customize it.

### Overview of task
Create a plugin that displays a list of movies, with the following tasks:  

- Create a JSON API from  a custom post type.
- Displays the movies as a frontpage (home page of the site) using the JSON API created in the previous task.

### Data / Specification
- Custom Post Type: Movie
- Fields / Meta Data of CPT
  - poster_url: a string to the url of an image associated with that movie
  - rating: a number rating / score of the value of that respective movie
  - year: date of release
  - description: short html description of the movie
- Page should automatically display on home page
- Logic for no movies, etc
- Simple documentation for using the plugin
- Structure should look like:

```json
{
  data: [
     {
        id: 1
        title: 'UP'
        poster_url: ‘http://localhost.dev/images/up.jpg’,
        rating: 5,
        year: 2010
        short_description: ‘Phasellus ultrices nulla quis nibh. Quisque a lectus',
     },
     {
        id: 2
        title: 'Avatar'
        poster_url: ‘http://localhost.dev/images/avatar.jpg’,
        rating: 3,
        year: 2012
        short_description: ‘Phasellus ultrices nulla quis nibh. Quisque a lectus',
     }
     …
  ]
}
```


### Bonus for
- Angular or other SPA frameworks for displaying movies
- Caching of the API for movies (cleared upon adding new movie)
- Fancy UI Effects / Animations / Etc.
- Follow WordPress coding standards
- PHP Unit tests
- TravisCI or Circle CI integration
