<?php
/*
Plugin Name: Moxie WP Test
Description: Movie manager with JSON frontend
Author: Caridad Zapatero Contreras
Version: 1.1
*/

class MoxieWPTest {

	/**
	* Setup actions and filters
	*/
	function __construct() {
		add_action( 'wp_ajax_movies', array( $this, 'movies' ) );
		add_action( 'wp_ajax_nopriv_movies', array( $this, 'movies' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_shortcode( 'movies', array( $this, 'movies_shortcode' ) );
		add_action( 'after_delete_post', array( $this, 'clear_cache' ) );
		add_action( 'save_post_movie', array( $this, 'clear_cache' ) );
	}

	/**
	* Output the HTML markup for videos.
	* This can be added to the theme template instead, for customization
	*/
	function movies_shortcode( $atts, $content = '' ) {
		return <<< EOT
<div id="movies" ng-app="moviesApp" ng-controller="MoviesCtrl">
	<div infinite-scroll="movies.nextPage()" infinite-scroll-disabled="movies.busy" infinite-scroll-distance="3">
		<div ng-repeat="movie in movies.items" class="movie wow fadeIn">
			<img alt="{{movie.title}}" title="{{movie.title}}" ng-src="{{movie.poster_url}}" />
			<div class="over" title="{{movie.title}}">
				<strong>{{movie.title}}</strong><br />
				<span class="year">Year: {{movie.year}}</span>
				<span class="dashicons dashicons-star-filled rating">{{movie.rating}}</span>
			</div>
		</div>
		<div ng-show="movies.busy">Loading data ...</div>
	</div>
</div>
EOT;
	}

	/**
	* Load Javascript and CSS
	*/
	function enqueue_scripts() {
		wp_register_script( 'angular', 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.0-beta.1/angular.js' );
		wp_register_script( 'ng-inifite-scroll', 'https://cdnjs.cloudflare.com/ajax/libs/ngInfiniteScroll/1.2.1/ng-infinite-scroll.min.js', array( 'angular', 'jquery' ) );
		wp_register_script( 'wow', plugins_url( '/js/wow.min.js', __FILE__ ) );
		wp_register_script( 'moxie-script', plugins_url( '/js/moxie.js', __FILE__ ), array( 'ng-inifite-scroll', 'wow' ) );
		wp_localize_script( 'moxie-script', 'moxie_vars', array(
			'ajax_url' => admin_url( 'admin-ajax.php?action=movies' )
		) );
		wp_enqueue_script( 'moxie-script' );
		wp_enqueue_style( 'dashicons' );
		wp_enqueue_style( 'moxie-css', plugins_url( '/css/moxie.css', __FILE__ ) );
		wp_enqueue_style( 'moxie-animate', plugins_url( '/css/animate.css', __FILE__ ) );
	}

	/**
	* Send a list of movies formatted as JSON for use in the frontpage
	*/
	function movies() {
		// Get page
		$after = intval( $_GET['after'] );
		// Get movies from cache
		if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
			// caching is disabled with WP_DEBUG enabled
			$data = array();
		} else {
			// get cached data
			$data = (array) get_transient( 'moxie_movies' );
		}
		// If not in cache, get from database
		if ( !isset( $data[ 'after' . $after ] ) ) {
			$movies = get_posts( array(
				'post_type' => 'movie',
				'meta_key' => 'wpcf-year',
				'orderby' => 'meta_value_num',
				'order' => 'desc',
				'posts_per_page' => 30,
				'offset' => $after
			) );
			// Format as required for JSON output
			$rows = array();
			foreach ( $movies as $movie ) {
				$rows[] = array(
					'id' => $movie->ID,
					'title' => $movie->post_title,
					'poster_url' => get_post_meta( $movie->ID, 'wpcf-poster_url', true ),
					'rating' => intval( get_post_meta( $movie->ID, 'wpcf-rating', true ) ),
					'year' => get_post_meta( $movie->ID, 'wpcf-year', true ),
					'short_description' => get_post_meta( $movie->ID, 'wpcf-description', true )
				);
			}
			// Keep in the cache for an hour
			$data[ 'after' . $after ] = $rows;
			set_transient( 'moxie_movies', $data, HOUR_IN_SECONDS );
		}
		// Output to the browser
		wp_send_json( $data[ 'after' . $after ] );
		wp_die();
	}

	/**
	* Clears cache when data changes
	*/
	function clear_cache() {
		delete_transient( 'moxie_movies' );
	}

}

// Create an instance of the plugin
new MoxieWPTest;
