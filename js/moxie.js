var moviesApp = angular.module('moviesApp', ['infinite-scroll']);

moviesApp.controller('MoviesCtrl', function($scope, Movies) {
	$scope.movies = new Movies();
});

moviesApp.factory('Movies', function($http) {
	var Movies = function() {
		this.items = [];
		this.busy = false;
		this.finished = false;
		this.after = 0;
	};

	Movies.prototype.nextPage = function() {
		if (this.busy || this.finished) {
			return;
		}
		this.busy = true;
		var url = moxie_vars.ajax_url + "&after=" + this.after;
		$http.get(url).success(function(items) {
			for (var i = 0; i < items.length; i++) {
				this.items.push(items[i]);
			}
			this.after = this.items.length;
			this.busy = false;
			if (items.length == 0) {
				this.finished = true;
	 		}
		}.bind(this));
	};

	return Movies;
});

new WOW().init();
